package com.empirix;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import static io.restassured.RestAssured.given;
import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Test;

@QuarkusTest
class HealthIT {

    @ConfigProperty(name="base.url")
    String ersUsers;
    
    @Test
    void testHealth()
    {
        given().when().get(String.format("%s/health",ersUsers)).then()
        .statusCode(200);

    }
}
