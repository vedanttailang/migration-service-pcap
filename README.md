# ERS-PCAP-LIBRARY

## Overview
ERS PCAP LIBRARY contains the following rest API applications:
* PCAP Library

## Get project and start development

## Prerequisites
*In order to start you must have a valid account on [gitlab](https://gitlab.com) that has enough privileges to see ewp-pcap project (you shouln't be able to read this if not ^_^)
* Have a valid ssh key to access git repository
* openjdk (with jdk) installed
* maven
* docker installed

## Get the code
In order to download code, in a bash shell run:
```shellscript
git clone git@gitlab.com:empirix-eng/ewp-ers/ers-pcap-library.git
```


## Clean 
To clean all target folders and restore a clean state:
```shellscript
mvn clean
```

## Build

Compile all code and create all artifacts 
```shellscript
mvn verify -Pdo-not-test
```
or if you want to start from a clean situation
```shellscript
mvn clean verify -Pdo-not-test
```


## Run test
Only unit tests, You can run both unit and integration tests with:
```shellscript
mvn test
```
All tests. If you want to run both unit and integration tests instead you can use
```shellscript
mvn verify
```
It's also possible to run single tests, see maven-surefire-plugin (unit tests) or maven-failsafe-plugin (integration tests) documentation for further details.

## Dependencies
* Minimal set of requisites to run ers-pcap-library (interface functional but error present in logs - and no audit):
```mermaid
graph TD;
  ERS-PCAP-LIBRARY-->EMS-OpenAM;
  ERS-PCAP-LIBRARY-->EMS-OpenDJ;
  EMS-OpenAM-->EMS-OpenDJ;
  ERS-PCAP-LIBRARY-->PostgreSQL;
```

* Runtime Dependencies
```mermaid
graph TD;
  ERS-PCAP-LIBRARY-->EMS-OpenAM;
  ERS-PCAP-LIBRARY-->EMS-OpenDJ;
  EMS-OpenAM-->EMS-OpenDJ;
  ERS-PCAP-LIBRARY-->PostgreSQL
  ERS-PCAP-LIBRARY-->Artemis;
  EMS-OpenAM-->Artemis;
  ERS-PCAP-LIBRARY-->CSA-NFV(data-reader);
```

## How to run

### prerequisites
* docker installed and configured properly
* ~/docker/config.json with your credentials for empirixartifactory.jfrog.io
* docker-compose installed
* make command available
* an entry on /etc/hosts for 127.0.0.1 pointing to portal.empirix.com

### run

* run into a bash shell
```shellscript
make up
```

### dev

* Running minimal dependencies (without authentication needed):
* run into a bash shell
```shellscript
make up_dev
```

* you can connect with your favourite browser to https://portal.empirix.com/pcaplibrary

### stop
* run into a bash shell
```shellscript
make down
```

## Logs
Below commands to see log information

* All logs together: 
```shelscripts
make logs
```
* Specific docker image log: 
```shellscript
docker log -f <imagename>
```


## Artifacts produced

* ers-pcap-library docker
* ers-pcap-library-test-bundle.zip a bundle that can be imported as maven dependency containing yml and all resources to include user-management in another project for testing purposes
* ers-pcap-library-helm.tar.gz helm template that can be used on kubernetes to deploy user-management pods.

## Helm details:

### Configuration

TBD

### Deploy helm 

```
helm install ers-pcap-library --name ers-pcap-library --namespace empirix [--debug]
```

### Uninstall

```
helm del --purge ers-pcap-library
```



