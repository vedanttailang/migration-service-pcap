package com.empirix.jwt;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.TimeUnit;

@ApplicationScoped
public class M2MTokenProvider
{
    private static final Logger                LOGGER    = LoggerFactory.getLogger(M2MTokenProvider.class);

    @ConfigProperty(name = "m2m.token.path")
    String                                     m2mTokenPath;

    private static final Cache<String, String> JWT_CACHE = Caffeine.newBuilder().initialCapacity(1).expireAfterWrite(1, TimeUnit.MINUTES).build();

    public String getJWTToken()
    {
        return JWT_CACHE.get(m2mTokenPath, M2MTokenProvider::readJWTToken);
    }

    private static String readJWTToken(final String tokenPath)
    {
        try
        {
            return Files.readString(Paths.get(tokenPath));
        }
        catch (final IOException e)
        {
            LOGGER.error("Error occurred retrieving M2M token from: {}, error: {}", tokenPath, e.getMessage());
            return null;
        }
    }
}
