package istio.authz
default allow = false

raw_request = input.attributes.request.http

baseToken = encoded {
    [_, encoded] := split(raw_request.headers.authorization, " ")
}

token = {"payload": payload} { io.jwt.decode(baseToken, [_, payload, _]) }

curr_request = { "path": path, "method": method} {
    path := raw_request.path
    method := raw_request.method
}

# collect interesting claims from jwt:
decoded_claims = {"name": user, "roles": roles} {
	user := token.payload.sub
    roles := {r| r = token.payload.groups[_]}
}

app_roles := {k| k:=data.role_based_resources[_].role}
denied_res := denied_rs(curr_request.method)

# check unprotected resources first:
allow {
    unprotected_resources := unprotected_rs(curr_request.method)
    check_match_with := unprotected_resources[_]
    re_match(check_match_with,curr_request.path) 
    not any_denied(curr_request.path)
} 

# check "authenticated_users" resources - i.e. resources that requires that the user is authenticated aka user has a jwt:
allow {
	re_match(".+", token.payload.sub)
    authenticated_resources := authenticated_rs(curr_request.method)
    check_match_with := authenticated_resources[_]
    re_match(check_match_with,curr_request.path)
    not any_denied(curr_request.path)
}
# Allow the action if the user has role that grants permission to perform the action.
allow {
    #grant := { uri | uri := data.role_based_resources[anentry].resources[_]; data.role_based_resources[anentry].methods[_] == curr_request.method; decoded_claims.roles[_] == data.role_based_resources[anentry].role  }
    grant := granted_rs(curr_request.method)
    check_match_with := grant[_]
    re_match(check_match_with,curr_request.path)  
    not any_denied(curr_request.path)
} 

# helpers:
any_denied(x) { 
    some j
	y = denied_res[j]
    re_match(y,x)
}

denied_rs(method) = denied {
  denied := { res | app_roles[currentRole]; not decoded_claims.roles[currentRole]; entry := data.role_based_resources[_]; entry.enforce_deny == true; entry.methods[_] == method; currentRole == entry.role; res:=entry.resources[_]}
}

granted_rs(method) = grant {
  grant := { uri | entry := data.role_based_resources[_]; entry.methods[_] == method; decoded_claims.roles[_] == entry.role; uri := entry.resources[_]  }
   
}

authenticated_rs(method) = res {
 res := { uri | uri := data.authenticated_resources[anentry].resources[_]; data.authenticated_resources[anentry].methods[_] == method}
}

unprotected_rs(method) = res {
 res := { uri | uri := data.unprotected_resources[anentry].resources[_]; data.unprotected_resources[anentry].methods[_] == method}
}

