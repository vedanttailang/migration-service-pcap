package istio.authz

#### ENDPOINT /health
test__grant_get_health_anonymous {
	allow with input as {"attributes": {"request": {"http": {"headers": {}, "method": "GET", "path": "/health"}}}}
}

#### ENDPOINT /settings:

test__denied_get_settings_anonymous {
	not allow with input as {"attributes": {"request": {"http": {"headers": {}, "method": "GET", "path": "/settings"}}}}
}

test__denied_put_settings_anonymous {
	not allow with input as {"attributes": {"request": {"http": {"headers": {}, "method": "PUT", "path": "/settings/333"}}}}
}

test__denied_delete_settings_anonymous {
	not allow with input as {"attributes": {"request": {"http": {"headers": {}, "method": "DELETE", "path": "/settings/332"}}}}
}

test__grant_get_settings_with_diagnostix_editor {
	token := io.jwt.encode_sign({"typ": "JWT", "alg": "HS256"}, {"groups": ["diagnostix_editor"], "sub": "test"}, {"kty": "oct", "k": "AyM1SysPpbyDfgZld3umj1qzKObwVMkoqQ-EstJQLr_T-1qS0gZH75aKtMN3Yj0iPS4hcgUuTwjAzZr1Z9CAow"})
	allow with input as {"attributes": {"request": {"http": {"headers": {"authorization": concat(" ", ["Bearer", token])}, "method": "GET", "path": "/settings"}}}}
}

test__denied_get_settings_ID_with_diagnostix_editor {
	token := io.jwt.encode_sign({"typ": "JWT", "alg": "HS256"}, {"groups": ["diagnostix_editor"], "sub": "test"}, {"kty": "oct", "k": "AyM1SysPpbyDfgZld3umj1qzKObwVMkoqQ-EstJQLr_T-1qS0gZH75aKtMN3Yj0iPS4hcgUuTwjAzZr1Z9CAow"})
	not allow with input as {"attributes": {"request": {"http": {"headers": {"authorization": concat(" ", ["Bearer", token])}, "method": "GET", "path": "/settings/dddd"}}}}
}

test__denied_get_settings_slash_with_diagnostix_editor {
	token := io.jwt.encode_sign({"typ": "JWT", "alg": "HS256"}, {"groups": ["diagnostix_editor"], "sub": "test"}, {"kty": "oct", "k": "AyM1SysPpbyDfgZld3umj1qzKObwVMkoqQ-EstJQLr_T-1qS0gZH75aKtMN3Yj0iPS4hcgUuTwjAzZr1Z9CAow"})
	not allow with input as {"attributes": {"request": {"http": {"headers": {"authorization": concat(" ", ["Bearer", token])}, "method": "GET", "path": "/settings/"}}}}
}

test__denied_delete_settings_ID_without_diagnostix_editor {
	token := io.jwt.encode_sign({"typ": "JWT", "alg": "HS256"}, {"groups": ["another_role"], "sub": "test"}, {"kty": "oct", "k": "AyM1SysPpbyDfgZld3umj1qzKObwVMkoqQ-EstJQLr_T-1qS0gZH75aKtMN3Yj0iPS4hcgUuTwjAzZr1Z9CAow"})
	not allow with input as {"attributes": {"request": {"http": {"headers": {"authorization": concat(" ", ["Bearer", token])}, "method": "GET", "path": "/settings/3333"}}}}
}

test__denied_delete_Settings_slash_with_diagnostix_editor {
	token := io.jwt.encode_sign({"typ": "JWT", "alg": "HS256"}, {"groups": ["diagnostix_editor"], "sub": "test"}, {"kty": "oct", "k": "AyM1SysPpbyDfgZld3umj1qzKObwVMkoqQ-EstJQLr_T-1qS0gZH75aKtMN3Yj0iPS4hcgUuTwjAzZr1Z9CAow"})
	not allow with input as {"attributes": {"request": {"http": {"headers": {"authorization": concat(" ", ["Bearer", token])}, "method": "DELETE", "path": "/settings/"}}}}
}

test__denied_post_Settings_ID_with_diagnostix_editor {
	token := io.jwt.encode_sign({"typ": "JWT", "alg": "HS256"}, {"groups": ["diagnostix_editor"], "sub": "test"}, {"kty": "oct", "k": "AyM1SysPpbyDfgZld3umj1qzKObwVMkoqQ-EstJQLr_T-1qS0gZH75aKtMN3Yj0iPS4hcgUuTwjAzZr1Z9CAow"})
	not allow with input as {"attributes": {"request": {"http": {"headers": {"authorization": concat(" ", ["Bearer", token])}, "method": "POST", "path": "/settings/3424"}}}}
}

test__grant_put_Settings_ID_with_diagnostix_editor {
	token := io.jwt.encode_sign({"typ": "JWT", "alg": "HS256"}, {"groups": ["diagnostix_editor"], "sub": "test"}, {"kty": "oct", "k": "AyM1SysPpbyDfgZld3umj1qzKObwVMkoqQ-EstJQLr_T-1qS0gZH75aKtMN3Yj0iPS4hcgUuTwjAzZr1Z9CAow"})
	allow with input as {"attributes": {"request": {"http": {"headers": {"authorization": concat(" ", ["Bearer", token])}, "method": "PUT", "path": "/settings/3424"}}}}
}

test__denied_put_Settings_ID_without_diagnostix_editor {
	token := io.jwt.encode_sign({"typ": "JWT", "alg": "HS256"}, {"groups": ["another_role"], "sub": "test"}, {"kty": "oct", "k": "AyM1SysPpbyDfgZld3umj1qzKObwVMkoqQ-EstJQLr_T-1qS0gZH75aKtMN3Yj0iPS4hcgUuTwjAzZr1Z9CAow"})
	not allow with input as {"attributes": {"request": {"http": {"headers": {"authorization": concat(" ", ["Bearer", token])}, "method": "PUT", "path": "/settings/3424"}}}}
}

test__denied_put_Settings_slash_with_diagnostix_editor {
	token := io.jwt.encode_sign({"typ": "JWT", "alg": "HS256"}, {"groups": ["diagnostix_editor"], "sub": "test"}, {"kty": "oct", "k": "AyM1SysPpbyDfgZld3umj1qzKObwVMkoqQ-EstJQLr_T-1qS0gZH75aKtMN3Yj0iPS4hcgUuTwjAzZr1Z9CAow"})
	not allow with input as {"attributes": {"request": {"http": {"headers": {"authorization": concat(" ", ["Bearer", token])}, "method": "PUT", "path": "/settings/"}}}}
}

### Endpoint /session-details

test__denied_get_session_details_anonymous {
	not allow with input as {"attributes": {"request": {"http": {"headers": {}, "method": "GET", "path": "/session-details"}}}}
}

test__denied_get_session_details_anonymous {
	not allow with input as {"attributes": {"request": {"http": {"headers": {}, "method": "GET", "path": "/session-details/33434"}}}}
}

test__denied_put_session_details_ID_anonymous {
	not allow with input as {"attributes": {"request": {"http": {"headers": {}, "method": "PUT", "path": "/session-details/333"}}}}
}

test__denied_delete_session_details_ID_anonymous {
	not allow with input as {"attributes": {"request": {"http": {"headers": {}, "method": "DELETE", "path": "/session-details/332"}}}}
}

test__denied_delete_session_details_ID_with_pcap_download {
	token := io.jwt.encode_sign({"typ": "JWT", "alg": "HS256"}, {"groups": ["pcap_download"], "sub": "test"}, {"kty": "oct", "k": "AyM1SysPpbyDfgZld3umj1qzKObwVMkoqQ-EstJQLr_T-1qS0gZH75aKtMN3Yj0iPS4hcgUuTwjAzZr1Z9CAow"})
	not allow with input as {"attributes": {"request": {"http": {"headers": {"authorization": concat(" ", ["Bearer", token])}, "method": "DELETE", "path": "/session-details/111"}}}}
}

test__denied_get_session_details_with_pcap_download {
	token := io.jwt.encode_sign({"typ": "JWT", "alg": "HS256"}, {"groups": ["pcap_download"], "sub": "test"}, {"kty": "oct", "k": "AyM1SysPpbyDfgZld3umj1qzKObwVMkoqQ-EstJQLr_T-1qS0gZH75aKtMN3Yj0iPS4hcgUuTwjAzZr1Z9CAow"})
	not allow with input as {"attributes": {"request": {"http": {"headers": {"authorization": concat(" ", ["Bearer", token])}, "method": "GET", "path": "/session-details"}}}}
}

test__denied_get_session_details_slash_with_pcap_download {
	token := io.jwt.encode_sign({"typ": "JWT", "alg": "HS256"}, {"groups": ["pcap_download"], "sub": "test"}, {"kty": "oct", "k": "AyM1SysPpbyDfgZld3umj1qzKObwVMkoqQ-EstJQLr_T-1qS0gZH75aKtMN3Yj0iPS4hcgUuTwjAzZr1Z9CAow"})
	not allow with input as {"attributes": {"request": {"http": {"headers": {"authorization": concat(" ", ["Bearer", token])}, "method": "GET", "path": "/session-details/"}}}}
}

test__grant_get_session_details_ID_with_pcap_download {
	token := io.jwt.encode_sign({"typ": "JWT", "alg": "HS256"}, {"groups": ["pcap_download"], "sub": "test"}, {"kty": "oct", "k": "AyM1SysPpbyDfgZld3umj1qzKObwVMkoqQ-EstJQLr_T-1qS0gZH75aKtMN3Yj0iPS4hcgUuTwjAzZr1Z9CAow"})
	allow with input as {"attributes": {"request": {"http": {"headers": {"authorization": concat(" ", ["Bearer", token])}, "method": "GET", "path": "/session-details/34343"}}}}
}

test__grant_get_session_details_with_pcap_viewer {
	token := io.jwt.encode_sign({"typ": "JWT", "alg": "HS256"}, {"groups": ["pcap_viewer"], "sub": "test"}, {"kty": "oct", "k": "AyM1SysPpbyDfgZld3umj1qzKObwVMkoqQ-EstJQLr_T-1qS0gZH75aKtMN3Yj0iPS4hcgUuTwjAzZr1Z9CAow"})
	allow with input as {"attributes": {"request": {"http": {"headers": {"authorization": concat(" ", ["Bearer", token])}, "method": "GET", "path": "/session-details"}}}}
}

test__denied_get_session_details_slash_with_pcap_viewer {
	token := io.jwt.encode_sign({"typ": "JWT", "alg": "HS256"}, {"groups": ["pcap_viewer"], "sub": "test"}, {"kty": "oct", "k": "AyM1SysPpbyDfgZld3umj1qzKObwVMkoqQ-EstJQLr_T-1qS0gZH75aKtMN3Yj0iPS4hcgUuTwjAzZr1Z9CAow"})
	not allow with input as {"attributes": {"request": {"http": {"headers": {"authorization": concat(" ", ["Bearer", token])}, "method": "GET", "path": "/session-details/"}}}}
}

test__grant_post_session_details_with_pcap_viewer {
	token := io.jwt.encode_sign({"typ": "JWT", "alg": "HS256"}, {"groups": ["pcap_viewer"], "sub": "test"}, {"kty": "oct", "k": "AyM1SysPpbyDfgZld3umj1qzKObwVMkoqQ-EstJQLr_T-1qS0gZH75aKtMN3Yj0iPS4hcgUuTwjAzZr1Z9CAow"})
	allow with input as {"attributes": {"request": {"http": {"headers": {"authorization": concat(" ", ["Bearer", token])}, "method": "POST", "path": "/session-details"}}}}
}

test__denied_post_session_details_slash_with_pcap_viewer {
	token := io.jwt.encode_sign({"typ": "JWT", "alg": "HS256"}, {"groups": ["pcap_viewer"], "sub": "test"}, {"kty": "oct", "k": "AyM1SysPpbyDfgZld3umj1qzKObwVMkoqQ-EstJQLr_T-1qS0gZH75aKtMN3Yj0iPS4hcgUuTwjAzZr1Z9CAow"})
	not allow with input as {"attributes": {"request": {"http": {"headers": {"authorization": concat(" ", ["Bearer", token])}, "method": "POST", "path": "/session-details/"}}}}
}

test__grant_post_session_details_id_with_pcap_viewer {
	token := io.jwt.encode_sign({"typ": "JWT", "alg": "HS256"}, {"groups": ["pcap_viewer"], "sub": "test"}, {"kty": "oct", "k": "AyM1SysPpbyDfgZld3umj1qzKObwVMkoqQ-EstJQLr_T-1qS0gZH75aKtMN3Yj0iPS4hcgUuTwjAzZr1Z9CAow"})
	allow with input as {"attributes": {"request": {"http": {"headers": {"authorization": concat(" ", ["Bearer", token])}, "method": "POST", "path": "/session-details/3333"}}}}
}

test__denied_post_session_details_id_with_pcap_download {
	token := io.jwt.encode_sign({"typ": "JWT", "alg": "HS256"}, {"groups": ["pcap_download"], "sub": "test"}, {"kty": "oct", "k": "AyM1SysPpbyDfgZld3umj1qzKObwVMkoqQ-EstJQLr_T-1qS0gZH75aKtMN3Yj0iPS4hcgUuTwjAzZr1Z9CAow"})
	not allow with input as {"attributes": {"request": {"http": {"headers": {"authorization": concat(" ", ["Bearer", token])}, "method": "POST", "path": "/session-details/3333"}}}}
}

test__denied_post_session_details_id_with_pcap_library_viewer {
	token := io.jwt.encode_sign({"typ": "JWT", "alg": "HS256"}, {"groups": ["pcap_library_viewer"], "sub": "test"}, {"kty": "oct", "k": "AyM1SysPpbyDfgZld3umj1qzKObwVMkoqQ-EstJQLr_T-1qS0gZH75aKtMN3Yj0iPS4hcgUuTwjAzZr1Z9CAow"})
	not allow with input as {"attributes": {"request": {"http": {"headers": {"authorization": concat(" ", ["Bearer", token])}, "method": "POST", "path": "/session-details/3333"}}}}
}

test__denied_post_session_details_slash_with_pcap_library_viewer {
	token := io.jwt.encode_sign({"typ": "JWT", "alg": "HS256"}, {"groups": ["pcap_library_viewer"], "sub": "test"}, {"kty": "oct", "k": "AyM1SysPpbyDfgZld3umj1qzKObwVMkoqQ-EstJQLr_T-1qS0gZH75aKtMN3Yj0iPS4hcgUuTwjAzZr1Z9CAow"})
	not allow with input as {"attributes": {"request": {"http": {"headers": {"authorization": concat(" ", ["Bearer", token])}, "method": "POST", "path": "/session-details/"}}}}
}

test__denied_get_session_details_with_pcap_library_viewer {
	token := io.jwt.encode_sign({"typ": "JWT", "alg": "HS256"}, {"groups": ["pcap_library_viewer"], "sub": "test"}, {"kty": "oct", "k": "AyM1SysPpbyDfgZld3umj1qzKObwVMkoqQ-EstJQLr_T-1qS0gZH75aKtMN3Yj0iPS4hcgUuTwjAzZr1Z9CAow"})
	not allow with input as {"attributes": {"request": {"http": {"headers": {"authorization": concat(" ", ["Bearer", token])}, "method": "POST", "path": "/session-details"}}}}
}

test__grant_get_session_details_with_pcap_library_viewer {
	token := io.jwt.encode_sign({"typ": "JWT", "alg": "HS256"}, {"groups": ["pcap_library_viewer"], "sub": "test"}, {"kty": "oct", "k": "AyM1SysPpbyDfgZld3umj1qzKObwVMkoqQ-EstJQLr_T-1qS0gZH75aKtMN3Yj0iPS4hcgUuTwjAzZr1Z9CAow"})
	allow with input as {"attributes": {"request": {"http": {"headers": {"authorization": concat(" ", ["Bearer", token])}, "method": "GET", "path": "/session-details"}}}}
}

test__denied_get_session_details_params_with_pcap_library_viewer {
	token := io.jwt.encode_sign({"typ": "JWT", "alg": "HS256"}, {"groups": ["pcap_library_viewer"], "sub": "test"}, {"kty": "oct", "k": "AyM1SysPpbyDfgZld3umj1qzKObwVMkoqQ-EstJQLr_T-1qS0gZH75aKtMN3Yj0iPS4hcgUuTwjAzZr1Z9CAow"})
	not allow with input as {"attributes": {"request": {"http": {"headers": {"authorization": concat(" ", ["Bearer", token])}, "method": "GET", "path": "/session-details?aaaa=aaa"}}}}
}

test__denied_get_session_details_id_with_pcap_library_viewer {
	token := io.jwt.encode_sign({"typ": "JWT", "alg": "HS256"}, {"groups": ["pcap_library_viewer"], "sub": "test"}, {"kty": "oct", "k": "AyM1SysPpbyDfgZld3umj1qzKObwVMkoqQ-EstJQLr_T-1qS0gZH75aKtMN3Yj0iPS4hcgUuTwjAzZr1Z9CAow"})
	not allow with input as {"attributes": {"request": {"http": {"headers": {"authorization": concat(" ", ["Bearer", token])}, "method": "GET", "path": "/session-details/3333"}}}}
}

test__denied_delete_session_details_slash_with_pcap_library_viewer {
	token := io.jwt.encode_sign({"typ": "JWT", "alg": "HS256"}, {"groups": ["pcap_library_viewer"], "sub": "test"}, {"kty": "oct", "k": "AyM1SysPpbyDfgZld3umj1qzKObwVMkoqQ-EstJQLr_T-1qS0gZH75aKtMN3Yj0iPS4hcgUuTwjAzZr1Z9CAow"})
	not allow with input as {"attributes": {"request": {"http": {"headers": {"authorization": concat(" ", ["Bearer", token])}, "method": "DELETE", "path": "/session-details/"}}}}
}

test__denied_delete_session_details_id_with_pcap_library_viewer {
	token := io.jwt.encode_sign({"typ": "JWT", "alg": "HS256"}, {"groups": ["pcap_library_viewer"], "sub": "test"}, {"kty": "oct", "k": "AyM1SysPpbyDfgZld3umj1qzKObwVMkoqQ-EstJQLr_T-1qS0gZH75aKtMN3Yj0iPS4hcgUuTwjAzZr1Z9CAow"})
	not allow with input as {"attributes": {"request": {"http": {"headers": {"authorization": concat(" ", ["Bearer", token])}, "method": "DELETE", "path": "/session-details/3333"}}}}
}

test__denied_put_session_details_id_with_pcap_library_viewer {
	token := io.jwt.encode_sign({"typ": "JWT", "alg": "HS256"}, {"groups": ["pcap_library_viewer"], "sub": "test"}, {"kty": "oct", "k": "AyM1SysPpbyDfgZld3umj1qzKObwVMkoqQ-EstJQLr_T-1qS0gZH75aKtMN3Yj0iPS4hcgUuTwjAzZr1Z9CAow"})
	not allow with input as {"attributes": {"request": {"http": {"headers": {"authorization": concat(" ", ["Bearer", token])}, "method": "PUT", "path": "/session-details/3333"}}}}
}

test__denied_put_session_details_slash_with_pcap_library_viewer {
	token := io.jwt.encode_sign({"typ": "JWT", "alg": "HS256"}, {"groups": ["pcap_library_viewer"], "sub": "test"}, {"kty": "oct", "k": "AyM1SysPpbyDfgZld3umj1qzKObwVMkoqQ-EstJQLr_T-1qS0gZH75aKtMN3Yj0iPS4hcgUuTwjAzZr1Z9CAow"})
	not allow with input as {"attributes": {"request": {"http": {"headers": {"authorization": concat(" ", ["Bearer", token])}, "method": "PUT", "path": "/session-details/"}}}}
}

test__denied_put_session_details_with_pcap_library_viewer {
	token := io.jwt.encode_sign({"typ": "JWT", "alg": "HS256"}, {"groups": ["pcap_library_viewer"], "sub": "test"}, {"kty": "oct", "k": "AyM1SysPpbyDfgZld3umj1qzKObwVMkoqQ-EstJQLr_T-1qS0gZH75aKtMN3Yj0iPS4hcgUuTwjAzZr1Z9CAow"})
	not allow with input as {"attributes": {"request": {"http": {"headers": {"authorization": concat(" ", ["Bearer", token])}, "method": "PUT", "path": "/session-details"}}}}
}

## TODO: pcap_library_public_loading
## TODO: pcap_library_public_delete
